const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true, 
        trim: true
    },
    password: {
        type: String,
        required: true,
        minLength: 7
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
})

userSchema.pre('save', async function (next) {
    // Hash the password before saving the user model
    if (this.isModified('password'))
        this.password = await bcrypt.hash(this.password, 8);
    next();
})

userSchema.methods.generateAuthToken = async function() {
    // Generate an auth token for the user
    const JWT_KEY = 'my-very-secret-string-that-I-have-to-put-in-env-var';
    const token = jwt.sign({_id: this._id}, JWT_KEY);
    this.tokens = this.tokens.concat({token});
    await this.save();
    return token;
}

userSchema.statics.findByCredentials = async (name, password) => {
    // Search for a user by name and password.
    const myUser = await user.findOne({name});
    if (!myUser) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    const isPasswordMatch = await bcrypt.compare(password, myUser.password);
    if (!isPasswordMatch) {
        throw new Error({ error: 'Invalid login credentials' });
    }
    return myUser;
}

const user = mongoose.model('User', userSchema);
module.exports = user;
