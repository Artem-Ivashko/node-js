const express = require('express')
var authController = require('../controllers/auth');

const router = express.Router()

router.post('/register', authController.post_register);
router.get('/login', authController.get_login);
router.post('/login', authController.post_login);

module.exports = router;
