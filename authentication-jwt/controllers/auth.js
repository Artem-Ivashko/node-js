const user = require('../models/user')

exports.post_register = async function(req, res) {
    // Create a new user
    try {
        const myUser = new user(req.body)
        await myUser.save()
        const token = await myUser.generateAuthToken()
        res.status(201).send({ token })
    } catch (error) {
        res.status(400).send(error)
    }
};

exports.get_login = function(req,res) {
    res.render('login');
};

exports.post_login = async function(req, res) {
    //Login a registered user
    try {
        const { name, password } = req.body
        const myUser = await user.findByCredentials(name, password)
        if (typeof myUser === 'undefined') {
            return res.status(401).send({error: 'Login failed! Check authentication credentials'})
        }
        const token = await myUser.generateAuthToken()
        res.send({ token })
    } catch (error) {
        res.status(400).send(error)
    }
};
