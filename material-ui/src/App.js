import React from 'react';

import { Typography, AppBar } from '@material-ui/core';
import { createMuiTheme, ThemeProvider, responsiveFontSizes } from '@material-ui/core/styles';

import Header from "components/Header/Header.js";


// Take look for choosing color palette: https://material.io/resources/color/
let myTheme = createMuiTheme({
  palette: {
    primary: { main: '#e3f2fd' },
    secondary: { main: '#ffb7bc' }
  }
});
myTheme = responsiveFontSizes(myTheme);

function App() {
  return (
      <ThemeProvider theme={myTheme}>
      <AppBar title="Enter Personal Details">
          <Typography variant="h6">
      News
    </Typography>
      </AppBar>
      </ThemeProvider>
  );
}

export default App;
